2.0.1 (25.08.2014)
 - New Cache Option
2.0.0 (23.08.2014)
 - New Version !
 - Add Cache Function
 - Work with http://trakt.tv/ (For US-Series and Movies)
1.2.1 (14.07.2014)
 - Fixed repository bug - -
1.2.0 (14.07.2014)
 - Fixed bugs
1.1.9 (13.07.2014)
 - Fixed bugs
1.1.8 (13.07.2014)
 - Add new Concert
 - Fixed US Series
 - Fixed Movies
 - Fixed TV Thailand
1.1.7 (16.04.2014)
 - Fixed TV Thailand
1.1.6 (15.04.2014)
 - Add new World Commentary
1.1.5 (14.04.2014)
 - Fixed bugs
 - Add new Sitcom - Thailand
 - Add new Kvariety
 - Add new AsianMovies
1.1.4 (20.12.2013)
 - Update TV - Thailand
1.1.3 (27.11.2013)
 - Fixed mini bug.
1.1.2 (26.11.2013)
 - Mini Update.
1.1.1 (23.11.2013)
 - Mini Update.
1.1.0 (22.10.2013)
 - New features update
 - add new kr series source
 - add new jp series source
 - add thai tv
1.0.1 (20.10.2013)
 - Fixed bugs - -"
1.0.0 (20.10.2013)
 - Release new menu option.
 - Minor changes.
 - Minor Bugs fixed.
0.0.4 (19.10.2013)
 - Fixed some bugs.
0.0.3 (9.9.2013)
 - Fix New version update links.
 - Thank you (https://www.facebook.com/chsw67) for this new update links.
 - Thank you (https://www.facebook.com/tang.hom.7) for this new update links.
0.0.2 (1.9.2013)
 - Fix search movies bug, report by (https://www.facebook.com/rewat.re)
0.0.1 (1.9.2013)
 - initial release
